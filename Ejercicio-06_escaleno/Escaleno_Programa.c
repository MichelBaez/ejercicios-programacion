/*
NOMBRE: Baez Cadena Diestefano Michel
PROPOSITO DEL PROGRAMA: Calcula el perimetro de un triangulo escaleno
FECHA: 14/08/2020
*/

#include <stdio.h>

int main()
{
	float a, b ,c, perimetro;
	char invalido[]="\nLONGITUD INVALIDA";
	
	printf("Este programa calcula el perimetro de un triangulo escaleno.\n");
	printf("Escribe la longitud del lado A:\t");
	scanf("%f",&a);
	if (a > 0){
		printf("Escribe la longitud del lado B:\t");
		scanf("%f", &b);
		if (b > 0){
			printf("Escribe la longitud del lado C:\t");
			scanf("%f", &c);
			if (c > 0){
				perimetro = a+b+c;
				printf("\nEl perimetro es igual a:\t%.2lf",perimetro);
			}else{
				printf("%s", invalido);
			}
		}else{
			printf("%s", invalido);
		}
	}else{
		printf("%s", invalido);
	}
	return 0;
}
