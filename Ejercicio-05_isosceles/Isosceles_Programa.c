/*
NOMBRE: Baez Cadena Diestefano Michel
PROPOSITO DEL PROGRAMA: Calcula el perimetro de un triangulo isosceles
FECHA: 14/08/2020
*/

#include <stdio.h>

int main()
{
	float a, b, perimetro;
	char invalido[]="\nLONGITUD INVALIDA";
	
	printf("Este programa calcula el perimetro de un triangulo equilatero.\n");
	printf("Escribe la longitud de la base(mayor a 0):\t");
	scanf("%f", &b);
	if (b > 0){
		printf("Escribe la longitud de los lados similares(mayor a 0):\t");
		scanf("%f", &a);
		if (a > 0){
			perimetro = (2*a)+b;
			printf("\nEl perimetro es igual a:\t%.2lf", perimetro);
		}else{
			printf("%s", invalido);
		}
	}else{
		printf("%s", invalido);
	}
	return 0;
}
