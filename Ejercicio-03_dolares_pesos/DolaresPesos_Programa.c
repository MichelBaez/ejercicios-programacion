/*
NOMBRE: Baez Cadena Diestefano Michel
PROPOSITO DEL PROGRAMA: Convierte una cantidad de d�lares a pesos mexicanos
FECHA: 14/08/2020
*/

#include <stdio.h>

int main()
{

	float pesos, dolar, valor=21.97;
	char invalido[]="\nCANTIDAD NO VALIDA";

	printf("Este programa te dice el valor de tus dolares en pesos mexicanos.\n");
	printf("Cada dolar vale %.2lf pesos\n",valor);
	printf("Ingresa la cantidad de dolares a convertir:\t");
	scanf("%f",&dolar);
	if  (dolar > 0){
		pesos = dolar * valor;
		printf("\n%.2lf dolares equivale a: %.2lf pesos", dolar, pesos);
	}else{
		printf("%s", invalido);
	}	
	return 0;
}
