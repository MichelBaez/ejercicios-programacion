/*
NOMBRE: Baez Cadena Diestefano Michel
PROPOSITO DEL PROGRAMA: Muestra la suma de los numeros desde el 1 hasta un numero de entrada
FECHA: 14/08/2020
*/

#include <stdio.h>

int main()
{
	
	int num, i, suma=0;
	char invalido[]="\nNUMERO INVALIDO!\nEl numero debe estar entre 1 y 50";
	
	printf("Este programa suma los numeros consecutivos desde el 1 hasta el n�mero que escriba el usuario.\n");
	printf("Escribe un numero entre el 1 y el 50:\t");
	scanf("%d", &num);
	if (num>=1 && num <= 50){
		for (i=1;i<=num;i++){
			suma=suma+i;
		}
		printf("\nLa suma es igual a: %d", suma);
	}else{
		printf("%s", invalido);
	}
	return 0;
}
