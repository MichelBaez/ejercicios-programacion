Inicio
	Variable lado es NUMERO REAL
	Variable perimetro es NUMERO REAL
	Variable invalido es CADENA DE CARACTERES

	lado = 0
	perimetro = 0
	invalido = "Numero invalido"

	Leer "Ingresa la longitud de un lado del triangulo:", lado
	Si (lado >= 0) entonces
		perimetro = lado * 3
		Escribir "El perimetro es igual a: ", perimetro
	Sino
		Escribir invalido
	Fin-Si
Fin