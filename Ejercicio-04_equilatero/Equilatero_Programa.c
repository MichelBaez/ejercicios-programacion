/*
NOMBRE: Baez Cadena Diestefano Michel
PROPOSITO DEL PROGRAMA: Calcula el perimetro de un triangulo equilatero
FECHA: 14/08/2020
*/

#include <stdio.h>

int main()
{
	float lado, perimetro;
	char invalido[]="\nLONGITUD INVALIDA";
	
	printf("Este programa calcula el perimetro d un triangulo equilatero.\n");
	printf("Escribe la longitud de un lado del triangulo:\t");
	scanf("%f", &lado);
	if (lado >= 0){
		perimetro = lado *3;
		printf("\nEl perimetro del triangulo es de: %.2lf",perimetro);
	}else{
		printf("%s", invalido);
	}
	return 0;
}
