/*
NOMBRE: Baez Cadena Diestefano Michel
PROPOSITO DEL PROGRAMA: Mostrar n�meros pares entre 0 y 100
FECHA: 14/08/2020
*/

#include <stdio.h>

int main()
{
	
	int i;
	
	printf("Este programa muestra todos los numeros pares entre el 0 y el 100.\n");
	printf("Los numeros pares son:\n");
	for(i=0;i<101;i++){
		if (i % 2 == 0)
			printf("%d\t",i);
	}
	return 0;
}
